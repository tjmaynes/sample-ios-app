install:
	bundle install --path vendor/bundle

build:
	bundle exec fastlane build

test:
	bundle exec fastlane test

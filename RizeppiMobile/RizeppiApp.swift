//
//  RizeppiApp.swift
//  Shared
//
//  Created by TJ Maynes on 7/22/21.
//

import SwiftUI

@main
struct RizeppiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

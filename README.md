# Rizeppi iOS

> A recipe book application.

## Requirements

- [GNU Make](https://www.gnu.org/software/make/)
- [Xcode](https://developer.apple.com/xcode/)
- [Ruby](https://www.ruby-lang.org/en/)

## Background

## Usage

To install project dependencies, run the following command:
```bash
make install
```

To build the project, run the following command:
```bash
make build
```

To run all tests, run the following command:
```bash
make test
```
